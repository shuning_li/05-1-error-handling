package com.twuc.webApp.controllers;

import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.security.AccessControlException;

@RestController
@RequestMapping("/api")
public class ExceptionController {
    @GetMapping("/exceptions/runtime-exception")
    public ResponseEntity<String> getException() {
        throw new RuntimeException();
    }

    @GetMapping("/exceptions/access-control-exception")
    public ResponseEntity<String> getAccessControlException() {
        throw new AccessControlException("access control exception");
    }

    @GetMapping("/errors/null-pointer")
    public ResponseEntity<String> getNullPointerException() {
        throw new NullPointerException();
    }

    @GetMapping("/errors/arithmetic")
    public ResponseEntity<String> getArithmeticException() {
        throw new ArithmeticException();
    }

    @ExceptionHandler({ NullPointerException.class, ArithmeticException.class })
    public ResponseEntity<String> handleNullPointerAndArithmeticException() {
        return ResponseEntity
                .status(HttpStatus.I_AM_A_TEAPOT)
                .contentType(MediaType.APPLICATION_JSON)
                .body("Something wrong with the argument");
    }

    @ExceptionHandler
    public ResponseEntity<String> handleAccessControlException(AccessControlException e) {
        return ResponseEntity
                .status(HttpStatus.FORBIDDEN)
                .body("handle AccessControlException");
    }

    @ExceptionHandler
    public ResponseEntity<String> handleRuntimeException(RuntimeException e) {
        return ResponseEntity
                .status(HttpStatus.NOT_FOUND)
                .body("handle RuntimeException");
    }
}
