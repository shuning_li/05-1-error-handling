package com.twuc.webApp.controllers;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/sister-errors")
public class SisterErrorController {
    @GetMapping("/illegal-argument")
    public ResponseEntity<String> getError() {
        throw new IllegalArgumentException();
    }
}
