package com.twuc.webApp;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.client.AutoConfigureWebClient;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@AutoConfigureWebClient
public class SisterErrorControllerTest {
    @Autowired
    TestRestTemplate template;

    @Test
    void should_get_error() {
        ResponseEntity<String> entity = template.getForEntity("/api/sister-errors/illegal-argument", String.class);
        Assertions.assertEquals(HttpStatus.I_AM_A_TEAPOT, entity.getStatusCode());
        Assertions.assertEquals("{\"message\": \"Something wrong with brother or sister.\"}", entity.getBody());
    }
}
