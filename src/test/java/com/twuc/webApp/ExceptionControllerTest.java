package com.twuc.webApp;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.client.AutoConfigureWebClient;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@AutoConfigureWebClient
public class ExceptionControllerTest {
    @Autowired
    TestRestTemplate template;

    @Test
    void should_get_status_500() {
        ResponseEntity<String> entity = template.getForEntity("/api/exceptions/runtime-exception", String.class);
        Assertions.assertEquals(HttpStatus.NOT_FOUND, entity.getStatusCode());
        Assertions.assertEquals("handle RuntimeException", entity.getBody());
    }

    @Test
    void should_get_null_pointer_exception() {
        ResponseEntity<String> entity = template.getForEntity("/api/errors/null-pointer", String.class);
        Assertions.assertEquals(HttpStatus.I_AM_A_TEAPOT, entity.getStatusCode());
        Assertions.assertEquals("Something wrong with the argument", entity.getBody());
    }

    @Test
    void should_get_arithmetic_exception() {
        ResponseEntity<String> entity = template.getForEntity("/api/errors/arithmetic", String.class);
        Assertions.assertEquals(HttpStatus.I_AM_A_TEAPOT, entity.getStatusCode());
        Assertions.assertEquals(MediaType.APPLICATION_JSON, entity.getHeaders().getContentType());
        Assertions.assertEquals("Something wrong with the argument", entity.getBody());
    }
}
