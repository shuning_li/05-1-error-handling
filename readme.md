d# 作业要求

## 1 Definition of Done

* 必须非常严格的按照题目中的定义进行实现。
* 每一种 case 都必须至少包含一个独立的测试。
* 所有的测试必须都成功。
* 注意代码的坏味道。尤其是命名的规范性以及重复代码的问题。

## 2 请书写测试来验证如下的 case

### 2.1 默认行为

请书写一个 API */api/errors/default* 在其中抛出一个 `RuntimeException` 实例。并不做任何处理，请验证默认情况下其 Response 的 Status Code。

### 2.2 Controller Level Handler

请书写一个 Controller Level 的 Exception Handler。假设目前有如下 API：*GET /api/errors/illegal-argument*

如果 method 抛出了 `IllegalArgumentException`，那么 Status Code 为 Internal Server Error。其响应的 content 为 "Something wrong with the argument"。请写出测试验证你的实现。

注意：不能由于 2.2 的存在而更改 2.1 的测试结果。

### 2.3 Controller Level Handler with Multiple Exception Types

请书写一个 Controller Level 的 Exception Handler。假设目前有如下两个 API：

* *GET /api/errors/null-pointer*
* *GET /api/errors/arithmetic*

如果第一个 method 抛出了 `NullPointerException` 而第二个 method 抛出 `ArithmeticException` ，那么 Status Code 为 418。其响应的 content 为 "Something wrong with the argument"。请写出测试验证你的实现。

注意：不能由于 2.3 的存在而更改 2.1、2.2 的测试结果。

### 2.4 Controller Advicer

现在有两个 Controller。其中一个 Controller 实现了 API：

* *GET /api/sister-errors/illegal-argument*，

而另一个  Controller 实现了 API：

* *GET /api/brother-errors/illegal-argument*

这两个实现中都抛出 `IllegalArgumentException`。

现在请书写 Exception Handler 方法（只能书写一个），令这两个 API 在抛出 `IllegalArgumentException` 时响应为：

|项目|说明|
|---|---|
|Status Code|418|
|Content-Type|application/json|
|Content|`{"message": "Something wrong with brother or sister."}`|